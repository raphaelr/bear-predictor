document.addEventListener("DOMContentLoaded", () => {
    const input = document.getElementById("upload");
    const output = document.getElementById("display");
    let objectUrl;

    input.addEventListener("change", async e => {
        if (objectUrl == null) {
            URL.revokeObjectURL(objectUrl);
        }
        const file = input.files[0];
        objectUrl = URL.createObjectURL(file);

        display.innerHTML = "";
        const img = document.createElement("img");
        img.src = objectUrl;
        display.appendChild(img);

        const prediction = await predict(file);
        const text = document.createTextNode(prediction);
        const textEl = document.createElement("h2");
        textEl.appendChild(text);
        display.appendChild(textEl);
    });
});

const trivia = {
    grizzly: "If it's brown, lie down.",
    black: "If it's black, fight back.",
    polar: "If it's white, good night.",
};

async function predict(file) {
    const form = new FormData();
    form.append("upload", file);

    const resp = await fetch("/predict", {
        method: "POST",
        body: form,
    });
    const data = await resp.json();
    const p = Math.round(100*data.p);
    return `This is a ${data.label} bear (${p}% confidence). ${trivia[data.label]}`;
}
