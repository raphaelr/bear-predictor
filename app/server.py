import json
import os

from bottle import get, post, static_file, request

from infer import predict

@get("/<filename>")
def http_static(filename):
    return static_file(filename, root = "./static")

@get("/")
def http_index():
    return http_static("index.html")

@post("/predict")
def http_predict():
    # data = { "label": "polar", "p": 0.995 }
    data = predict(request.files.get("upload").file.read())
    return json.dumps(data)

