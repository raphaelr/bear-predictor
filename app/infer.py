from fastai.vision.all import *

learner = load_learner("bears.pkl")

def predict(img):
    x = learner.predict(img)
    y = { "label": str(x[0]), "p": x[2][x[1].item()].item() }
    print(y)
    return y

