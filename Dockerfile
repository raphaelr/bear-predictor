FROM pytorch/pytorch
RUN pip install bottle fastai
COPY app /app
WORKDIR /app
RUN python3 server.py

EXPOSE 8000
ENTRYPOINT python3 main.py
